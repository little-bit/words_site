from django.contrib import admin

# Register your models here.


# Register your models here.
from .models import User, EmailVerifyRecord,UserErrorBook


class UserAdmin(admin.ModelAdmin):
    list_display = ("username","email","is_superuser","is_staff","icon")
    list_filter = ["is_superuser","is_staff"]

admin.site.register(User, UserAdmin)


class EmailAdmin(admin.ModelAdmin):
    list_display = ['code', 'email', 'send_time', 'email_type']
    search_fields = ['code', 'email']
    list_filter = ['send_time']
admin.site.register(EmailVerifyRecord, EmailAdmin)

class ErrorBookAdmin(admin.ModelAdmin):
    list_display = ['username', 'content', 'meaning', 'time']
    search_fields = ['username', 'time']
    list_filter = ['time']
admin.site.register(UserErrorBook,ErrorBookAdmin)
