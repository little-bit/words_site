from datetime import datetime, timedelta

from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from .models import EmailVerifyRecord,UserErrorBook
User = get_user_model()

class EmailSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True,help_text="密码")
    email_type = serializers.ChoiceField(choices=(('register', '注册邮件'), ('forget', '找回密码')),
        required=True,help_text="验证码类型")

    class Meta:
        model = EmailVerifyRecord
        fields = ("emal_type", "email")


class CodeSerlializer(serializers.ModelSerializer):

    code = serializers.CharField(required=True,min_length=4,max_length=4,help_text="验证码")
    username = serializers.CharField(label="用户名", help_text="用户名", required=True, allow_blank=False,
                                     validators=[UniqueValidator(queryset=User.objects.all(), message="用户已经存在")])
    email = serializers.EmailField(required=True,
                                   validators=[UniqueValidator(queryset=User.objects.all(), message="邮箱已经注册")])
    password = serializers.CharField(
        style={'input_type': 'password'}, help_text="密码",label="密码", min_length=8
    )
    def validate_code(self, code):

        if EmailVerifyRecord.objects.filter(code=code,email=self.initial_data["email"]):
            pass
        else:
            return serializers.ValidationError("验证码错误")

    class Meta:
        model = User
        fields = ("username", "code", "email", "password",)



class UserInfoSerializer(serializers.ModelSerializer):

    username=serializers.CharField()
    email=serializers.EmailField()
    icon=serializers.ImageField()
    gender=serializers.BooleanField()
    nickname = serializers.CharField()

    class Meta:
        model = User
        fields = ("id","username", "gender", "icon","email", "nickname")


class RegisterSerializer(serializers.Serializer):

    username=serializers.CharField(required=False)
    email=serializers.EmailField(required=False)
    password=serializers.CharField(required=True)

    class Meta:

        model=User
        fields=("username","email","password")

class ErrorBookSerializer(serializers.ModelSerializer):

    username=serializers.CharField(max_length=50,required=True)
    content=serializers.CharField(max_length=100,required=False)
    meaning=serializers.CharField(max_length=200,required=False)

    def validate_username(self, username):
        if User.objects.filter(username=username):
            return username
        else:
            raise serializers.ValidationError("用户不存在")

    class Meta:

        model=UserErrorBook
        fields=("username","content","meaning")

class DelErrorBookSerializer(serializers.ModelSerializer):

    class Meta:
        model=UserErrorBook
        fields="__all__"

class PasswordSerializer(serializers.ModelSerializer):

    password=serializers.CharField(required=True)
    code = serializers.CharField(required=True)
    email=serializers.EmailField(required=True)

    def validate_code(self, code):
        if EmailVerifyRecord.objects.filter(code=code,email=self.initial_data["email"]):
            del code
            newpassword= make_password(self.initial_data["password"], None, 'md5')
            User.objects.filter(email=self.initial_data["email"]).update(password=newpassword)

        else:
            return serializers.ValidationError("验证码错误")

    class Meta:
        model=User
        fields=("email","password","code")
