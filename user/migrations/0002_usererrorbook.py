# Generated by Django 2.2 on 2020-03-09 21:52

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserErrorBook',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('username', models.CharField(max_length=50, verbose_name='用户名')),
                ('content', models.CharField(max_length=100, verbose_name='内容')),
                ('meaning', models.CharField(max_length=200, verbose_name='词义')),
                ('time', models.DateTimeField(default=datetime.datetime.now, verbose_name='添加时间')),
            ],
            options={
                'verbose_name': '错题本',
                'verbose_name_plural': '错题本',
            },
        ),
    ]
