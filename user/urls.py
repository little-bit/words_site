from django.urls import path, include
from rest_framework.authtoken.views import obtain_auth_token
from . import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'code',views.SendEmailcodeView,basename="用户详情")

urlpatterns = [
    path('', include(router.urls)),
    path('register',views.RegisterViews.as_view()),
    path('getuser/<str:username>',views.GetUserViewSet.as_view()),
    path('update/<str:username>',views.UpdateUserInfoView.as_view()),
    path('login',obtain_auth_token),
    path('errorbook',views.UserErrorBookView.as_view()),
    path('delerr/<str:username>',views.DeleteAllOfErrorBookView.as_view()),
    path('changepass',views.ChangePasswordView.as_view()),

]