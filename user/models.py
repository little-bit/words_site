from datetime import datetime

from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.
class User(AbstractUser):

    icon=models.ImageField(verbose_name="头像",default="icon/icon.png",upload_to="icon")
    gender=models.BooleanField(verbose_name="性别",default=False)
    nickname = models.CharField(max_length=50, blank=True,verbose_name="昵称")

    class Meta(AbstractUser.Meta):
        verbose_name="用户信息"
        verbose_name_plural=verbose_name

class EmailVerifyRecord(models.Model):
    # 验证码
    code = models.CharField(max_length=20, verbose_name='验证码')
    # 用户邮箱
    email = models.EmailField(max_length=50, verbose_name='用户邮箱')
    send_time = models.DateTimeField(default=datetime.now, verbose_name='发送时间', null=True, blank=True)
    # 过期时间
    exprie_time = models.DateTimeField(null=True)
    # 邮件类型
    # choices 枚举选项, 必须从指定的项中选择一个
    email_type = models.CharField(choices=(('register', '注册邮件'), ('forget', '找回密码')), max_length=10)

    class Meta:
        verbose_name = '邮件验证码'
        verbose_name_plural=verbose_name

class UserErrorBook(models.Model):

    id=models.AutoField(primary_key=True)
    username=models.CharField(max_length=50,verbose_name="用户名",default=None)
    content=models.CharField(max_length=100,verbose_name="内容",default="a")
    meaning=models.CharField(max_length=200,verbose_name="词义",default="一个，")
    time=models.DateTimeField(default=datetime.now,verbose_name="添加时间")

    class Meta:
        verbose_name = '错题本'
        verbose_name_plural=verbose_name

