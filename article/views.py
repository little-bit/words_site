from django.shortcuts import render
from .models import Article,WordBank
from rest_framework import mixins,generics
from .serializers import ArticleSerializer,WordBankSerlizer
# Create your views here.

class ArticleView(mixins.ListModelMixin,generics.GenericAPIView):

    queryset = Article.objects.all()
    serializer_class = ArticleSerializer

    def get(self,request,*args,**kwargs):

        return  self.list(request,*args,**kwargs)


class WordBankView(mixins.ListModelMixin,generics.GenericAPIView):

    queryset = WordBank.objects.all()
    serializer_class = WordBankSerlizer

    def get(self,request,*args,**kwargs):

        return self.list(request,*args,**kwargs)
