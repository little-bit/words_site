from django.db import models
import datetime
from django_summernote.fields import SummernoteTextField
# Create your models here.

class Article(models.Model):

    id=models.AutoField(primary_key=True)
    title=models.CharField(max_length=100,verbose_name="标题")
    content=SummernoteTextField(verbose_name='内容',max_length=3000)
    summary=models.CharField(max_length=100,verbose_name="简介")
    img=models.ImageField(verbose_name="图片",upload_to='article')
    auther=models.CharField(max_length=10,verbose_name="作者",default="官方")
    createdTime=models.DateTimeField(default=datetime.datetime.now,verbose_name="发表时间")

    class Meta:
        verbose_name="精美文章"
        verbose_name_plural=verbose_name

class WordBank(models.Model):

    id = models.AutoField(primary_key=True)
    wordmeaning=models.CharField(max_length=100,verbose_name="词义")
    word=models.CharField(max_length=30,verbose_name="单词")
    sentene=models.CharField(max_length=400,verbose_name="例句")
    time=models.DateTimeField(default=datetime.datetime.now,verbose_name="添加时间")

    class Meta:
        verbose_name="单词词库"
        verbose_name_plural=verbose_name


