from .models import Article,WordBank
from rest_framework import serializers


class ArticleSerializer(serializers.ModelSerializer):

    class Meta:
        model=Article
        fields="__all__"

class WordBankSerlizer(serializers.ModelSerializer):

    class Meta:
        mode=WordBank
        fields="__all__"