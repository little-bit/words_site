
from django.urls import path
from .views import ArticleView,WordBankView


urlpatterns = [
  path('article',ArticleView.as_view()),
  path('words',WordBankView.as_view())
]