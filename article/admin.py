from django.contrib import admin

# Register your models here.
from .models import Article,WordBank


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title','createdTime','summary','img')
    list_filter = ['title','createdTime']



admin.site.register(Article, ArticleAdmin)

class WordAdmin(admin.ModelAdmin):
    list_display = ('word','wordmeaning','sentene','time')
    list_filter = ['time','word']



admin.site.register(WordBank, WordAdmin)