# Generated by Django 2.2 on 2020-03-10 16:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0002_wordbank'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='wordbank',
            options={'verbose_name': '单词词库', 'verbose_name_plural': '单词词库'},
        ),
    ]
