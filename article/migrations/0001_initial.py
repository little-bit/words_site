# Generated by Django 2.2 on 2020-03-09 20:58

import datetime
from django.db import migrations, models
import django_summernote.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=100, verbose_name='标题')),
                ('content', django_summernote.fields.SummernoteTextField(max_length=3000, verbose_name='内容')),
                ('summary', models.CharField(max_length=100, verbose_name='简介')),
                ('img', models.ImageField(upload_to='article', verbose_name='图片')),
                ('auther', models.CharField(default='官方', max_length=10, verbose_name='作者')),
                ('createdTime', models.DateTimeField(default=datetime.datetime.now, verbose_name='发表时间')),
            ],
            options={
                'verbose_name': '精美文章',
                'verbose_name_plural': '精美文章',
            },
        ),
    ]
